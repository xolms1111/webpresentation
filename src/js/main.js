"use strict";
var translate = 0;
var activeslide = 1; //Текущий слайдер
var slidetime = 5000; //Скорость смены слайда
var slidercount = $('.content__item').length; //Кол-во слайдов
var sliderwidth = 100 * slidercount; //Ширина всего слайдера
var slidewidth = 100 / slidercount; // Ширина слайда
var sliderwrap = $('.content__wrapper'); //Сам слайдер
var slideitem = $('.content__item'); //Сам слайд
var head_height = $('.page__head').height(); //Высота шапки
var nav_height = $('.nav').height(); //Высота навигации
var interval = setInterval(Slide, slidetime);
sliderwrap.css('width', sliderwidth + "%");
slideitem.css({
	'width': slidewidth + "%",
	'height': $(document).height() - head_height - nav_height - 95
});

function Slide() {
	if (activeslide == slidercount || activeslide <= 0 || activeslide > slidercount) { // если истина то возвращаемся на 1-й слайд
		sliderwrap.css({
			'transform': 'translate(' + 0 + 'px, 0)',
			'-webkit-transform': 'translate(' + 0 + 'px, 0)',
			'-ms-transform': 'translate(' + 0 + 'px, 0)'
		})
		activeslide = 1;
		console.log(activeslide);
	}
	else { // в ином случае переходим на следующий слайд
		translate = -$('.content__viewport').width() * (activeslide);
		sliderwrap.css({
			'transform': 'translate(' + translate + 'px, 0)',
			'-webkit-transform': 'translate(' + translate + 'px, 0)',
			'-ms-transform': 'translate(' + translate + 'px, 0)'
		});
		activeslide++;
		console.log(activeslide);
	}
}

function getSlide(slidebutton) {
	var slideindex = $(".content__item[data-slider=" + slidebutton + "]").index();
	var currentslide = slideindex + 1;
	if (currentslide > activeslide) {
		clearInterval(interval);
		interval = setInterval(Slide, slidetime);
		translate = -$('.content__viewport').width() * (currentslide - 1);
		sliderwrap.css({
			'transform': 'translate(' + translate + 'px, 0)',
			'-webkit-transform': 'translate(' + translate + 'px, 0)',
			'-ms-transform': 'translate(' + translate + 'px, 0)'
		});
	}
	else if (currentslide < activeslide) {
		clearInterval(interval);
		interval = setInterval(Slide, slidetime);
		translate = -$('.content__viewport').width() * (activeslide - (activeslide - currentslide) - 1);
		sliderwrap.css({
			'transform': 'translate(' + translate + 'px, 0)',
			'-webkit-transform': 'translate(' + translate + 'px, 0)',
			'-ms-transform': 'translate(' + translate + 'px, 0)'
		, });
	}
	activeslide = currentslide;
	//}
	//});
}
$(document).ready(function () {
	Slide();
	getSlide();
});
